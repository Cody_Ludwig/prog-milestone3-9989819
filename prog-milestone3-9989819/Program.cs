﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_9989819
{
    class Program
    {
        static void Main(string[] args)
        {
            int mainMenu;
            string pizzaType;
            int i;
            int x;
            var type = new List<string> { "Meatlovers", "Chicken & Bacon", "The Works", "Vegetarian" };
            var order = new List<Tuple<string, string, double>>();
            double total = 0;
            string owing;
            double cashPaid = 0;

            List<Tuple<string, double>> pizzas = new List<Tuple<string, double>>();
            pizzas.Add(Tuple.Create("Small", 4.99));
            pizzas.Add(Tuple.Create("Regular", 9.99));
            pizzas.Add(Tuple.Create("Family", 17.29));

            List<Tuple<string, double>> drinks = new List<Tuple<string, double>>();
            drinks.Add(Tuple.Create("Pepsi", 2.49));
            drinks.Add(Tuple.Create("Moutain Dew", 2.99));
            drinks.Add(Tuple.Create("Orange Juice", 4.99));


        Menustart:
            Console.Clear();

            Console.WriteLine("*********************************************");
            Console.WriteLine();
            Console.WriteLine("Welcome to Rustled Jimmies Pizza.");
            Console.WriteLine();
            Console.WriteLine("Would you like to enter your details now or after your order?");
            Console.WriteLine();
            Console.WriteLine("1. Enter details now");
            Console.WriteLine("2. Enter details later");
            Console.WriteLine();
            Console.WriteLine("*********************************************");
            if (int.TryParse(Console.ReadLine(), out mainMenu))
            {
              switch (mainMenu)
               {
                case 1:
                  Customer.custDetails();

                  break;

                case 2:

                  goto orderMenu;

                default:
                  {
                     Console.WriteLine();
                     Console.WriteLine("Invalid: Press any key.");
                     Console.ReadKey();
                     goto Menustart;
                  }

               }
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Invalid: Press any key.");
                Console.ReadKey();
                goto Menustart;
            }
        orderMenu:
            
            Console.Clear();
            Order.ShowOrder();
            Console.WriteLine();
            Console.WriteLine("Select an option");
            Console.WriteLine("1. Order Pizza");
            Console.WriteLine("2. Order Drinks");
            Console.WriteLine("3. Confirm order");
            Console.WriteLine();
            Console.WriteLine("*********************************************");

            if (int.TryParse(Console.ReadLine(), out x))
            {
                switch (x)
                {
                    case 1:
                    Pizzastart:
                        Console.Clear();
                        Console.WriteLine();
                        Console.WriteLine("Current Order:");
                        Order.ShowOrder();
                        for (i = 0; i < type.Count; i++)
                        {
                            Console.WriteLine($"{i + 1}. {type[i]}");
                        }
                        Console.WriteLine();
                        Console.WriteLine($"{i + 1}. Return to menu");
                        Console.WriteLine("*********************************************");
                        if (int.TryParse(Console.ReadLine(), out x))
                        {
                            if (x == i + 1)
                            {
                                goto orderMenu;
                            }
                            else if (x > i + 1)
                            {
                                Console.WriteLine();
                                Console.WriteLine("Invalid: Press any key");
                                Console.ReadKey();
                                goto Pizzastart;
                            }
                            else
                            {
                                pizzaType = type[x - 1];
                                Console.Clear();
                                Order.ShowOrder();
                                for (i = 0; i < pizzas.Count; i++)
                                {
                                    Console.WriteLine($"{i + 1}. {pizzas[i].Item1}");
                                }
                                Console.WriteLine();
                                Console.WriteLine($"{i + 1}. Return to menu");
                                Console.WriteLine("*********************************************");
                                if (int.TryParse(Console.ReadLine(), out x))
                                {
                                    if (x == i + 1)
                                    {
                                        goto orderMenu;
                                    }
                                    else if (x > i + 1)
                                    {
                                        Console.WriteLine();
                                        Console.WriteLine("Invalid: Press any key");
                                        Console.ReadKey();
                                        goto Pizzastart;
                                    }
                                    else
                                    {
                                        order.Add(Tuple.Create(pizzaType, pizzas[x - 1].Item1, pizzas[x - 1].Item2));
                                        Order.order.Add(Tuple.Create(pizzaType, pizzas[x - 1].Item1, pizzas[x - 1].Item2));
                                    }
                                    goto Pizzastart;
                                }
                                else
                                {
                                    Console.WriteLine();
                                    Console.WriteLine("Invalid: Press any key");
                                    Console.ReadKey();
                                    goto Pizzastart;
                                }
                            }

                        }
                        else
                        {
                            Console.WriteLine();
                            Console.WriteLine("Invalid: Press any key");
                            Console.ReadKey();
                            goto Pizzastart;
                        }


                    case 2:
                    Drinkstart:

                        Console.Clear();

                        Order.ShowOrder();
                        for (i = 0; i < drinks.Count; i++)
                        {
                            Console.WriteLine($"{i + 1}. {drinks[i].Item1}");
                        }
                        Console.WriteLine();
                        Console.WriteLine($"{i + 1}. Return to menu");
                        Console.WriteLine("*********************************************");
                        if (int.TryParse(Console.ReadLine(), out x))
                        {
                            if (x == i + 1)
                            {
                                goto orderMenu;
                            }
                            else if (x > i + 1)
                            {
                                Console.WriteLine();
                                Console.WriteLine("Invalid: Press any key");
                                Console.ReadKey();
                                goto Drinkstart;
                            }
                            else
                            {
                                order.Add(Tuple.Create("Drink     ", drinks[x - 1].Item1, drinks[x - 1].Item2));
                                Order.order.Add(Tuple.Create("Drink     ", drinks[x - 1].Item1, drinks[x - 1].Item2));
                            }
                            goto Drinkstart;
                        }
                        else
                        {
                            Console.WriteLine();
                            Console.WriteLine("Invalid: Press any key.");
                            Console.ReadKey();
                            goto Drinkstart;
                        }

                    case 3:
                    Confirm:
                        if (Customer.phone == 0)
                        {
                            Customer.custDetails();
                            goto Confirm;
                        }
                        else
                        {

                            Console.Clear();
                            Order.ShowOrder();
                            Console.WriteLine("Is this order Correct?");
                            Console.WriteLine("1. Yes");
                            Console.WriteLine("2. No");
                            if (int.TryParse(Console.ReadLine(), out x))
                            {
                                switch (x)
                                {
                                    case 1:
                                        Console.Clear();
                                        for (i = 0; i < order.Count; i++)
                                        {
                                            owing = String.Format("{0:C}", order[i].Item3);
                                            Console.WriteLine($"{order[i].Item1}      {order[i].Item2}      {owing}");
                                            total = total + order[i].Item3;
                                        }
                                        owing = String.Format("{0:C}", total);
                                        Console.WriteLine();
                                        Console.WriteLine($"                TOTAL:              ${total}");
                                        Console.WriteLine("*********************************************");
                                        Console.WriteLine();
                                        Console.WriteLine("Please enter amout of cash you wish to pay with");
                                        Console.WriteLine();
                                        cashPaid = double.Parse(Console.ReadLine());

                                        if (cashPaid > total)
                                        {
                                            cashPaid = cashPaid - total;
                                            Console.WriteLine($"Thank you, your change is ${cashPaid}");
                                        }
                                        else if (cashPaid == total)
                                        {
                                            Console.WriteLine("Thank you, enjoy");
                                        }
                                        else if (cashPaid < total)
                                        {
                                            total = total - cashPaid;
                                            Console.WriteLine("Declined, please try agian later");
                                        }
                                        Environment.Exit(0);
                                        break;

                                    case 2:
                                        goto orderMenu;
                                }
                            }




                        }
                        break;

                    default:
                        Console.WriteLine("Invalid: Press any key");
                        Console.Read();
                        goto orderMenu;




                }
            }
            else
            {
                Console.WriteLine("Invalid: Press any key");
                Console.Read();
                goto orderMenu;
            }

        }
    }

    public static class Order
    {

        public static List<Tuple<string, string, double>> order = new List<Tuple<string, string, double>>();

        public static void ShowOrder()
        {
            int i;
            double total = 0;
            string owing;

            Console.WriteLine("*********************************************");
            Console.WriteLine($"Name: {Customer.name}");
            Console.WriteLine($"Phone: {Customer.displayphone}");
            Console.WriteLine("*********************************************");
            Console.WriteLine("Order Summary:");

            for (i = 0; i < order.Count; i++)
            {
                owing = String.Format("{0:C}", order[i].Item3);
                Console.WriteLine($"{order[i].Item1}      {order[i].Item2}      {owing}");
                total = total + order[i].Item3;
            }
            owing = String.Format("{0:C}", total);
            Console.WriteLine();
            Console.WriteLine($"                TOTAL:              ${total}");
            Console.WriteLine("*********************************************");
        }
        
    }


    public static class Customer
    {
        public static string name;
        public static int phone;
        public static string displayphone;


        public static void custDetails()
        {
            int x;
            Start:
            Console.Clear();
            Console.WriteLine("*********************************************");
            Console.WriteLine();
            Console.WriteLine("Enter your name for the order");
            Console.WriteLine();

            name = Console.ReadLine();

            Console.WriteLine();
            Console.WriteLine("Enter your phone number");
            Console.WriteLine();
            if (int.TryParse(Console.ReadLine(), out phone))
            {
                displayphone = ($"0{phone}");
                Console.WriteLine();
                Console.WriteLine($"Order name: {name}");
                Console.WriteLine($"Order phone number: {displayphone}");
                Console.WriteLine("");
                Console.WriteLine("Confirm details");
                Console.WriteLine("1. Correct");
                Console.WriteLine("2. Change");
                Console.WriteLine();

                if (int.TryParse(Console.ReadLine(), out x))
                {
                    switch (x)
                    {
                        case 1:
                            break;

                        default:
                            goto Start;
                    }
                }
                else
                {
                    Console.WriteLine();
                    Console.WriteLine("Invalid: Press any key");
                    Console.ReadKey();
                    goto Start;
                }

            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Invalid: Press any key");
                Console.ReadKey();
                goto Start;
            }


        }

    }



}
